import 'package:first_app/widgets/app_body_widget.dart';
import 'package:first_app/widgets/app_bottom_bar_widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.grey.shade300,
        appBar: AppBar(
          backgroundColor: Colors.blue.shade800,
          title: const Text('Олександр'),
        ),
        drawer: Drawer(
          child: ListView(
            children: const [
              AppBodyWidget(),
              AppBodyWidget(),

            ],
          )
        ),
        body: const AppBodyWidget(),
        bottomNavigationBar: const AppBottomBarWidget(),
      ),
    ),
  );
}
