import 'package:flutter/material.dart';

class AppBodyWidget extends StatelessWidget {
  const AppBodyWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
      child: Column(
        children: [
          const SizedBox(height: 12),
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(12)),
            padding: const EdgeInsets.all(16),
            child: const _AppBodyInfo(title: 'Ваше місто: Одеса', icon: Icons.add_location),
          ),
          const SizedBox(height: 12),
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(12)),
            padding: const EdgeInsets.all(16),
            child:
            const _AppBodyInfo(title: 'Магазини', icon: Icons.shopping_cart_rounded),
          ),
          const SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Text('Замовлення',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 17,
                      fontWeight: FontWeight.w700)),
            ],
          ),
          const SizedBox(height: 12),
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(12)),
            padding: const EdgeInsets.all(16),
            child: Column(
              children: const [
                _AppBodyInfo(title: 'Усі замовлення', icon: Icons.markunread_mailbox_rounded),
                SizedBox(height: 24),
                _AppBodyInfo(title: 'Порівняння', icon: Icons.difference),
                SizedBox(height: 24),
                _AppBodyInfo(title: 'Переглянуті товари', icon: Icons.history),
              ],
            ),
          ),
          const SizedBox(height: 24),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const [
              Text('Профіль',
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 17,
                      fontWeight: FontWeight.w700)),
            ],
          ),
          const SizedBox(height: 12),
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(12)),
            padding: const EdgeInsets.all(16),
            child: Column(
              children: const [
                _AppBodyInfo(title: 'Інформація', icon: Icons.person),
                SizedBox(height: 24),
                _AppBodyInfo(title: 'Мої організації', icon: Icons.house),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _AppBodyInfo extends StatelessWidget {
  const _AppBodyInfo({Key? key, required this.title, required this.icon})
      : super(key: key);
  final String title;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          color: Colors.blue.shade800,
        ),
        const SizedBox(
          width: 12,
        ),
        Text(
          title,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 15,
            fontWeight: FontWeight.w500,
          ),
        )
      ],
    );
  }
}
