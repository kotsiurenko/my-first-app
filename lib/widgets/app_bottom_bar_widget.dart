import 'package:flutter/material.dart';

class AppBottomBarWidget extends StatefulWidget {
  const AppBottomBarWidget({Key? key}) : super(key: key);

  @override
  State<AppBottomBarWidget> createState() => _AppBottomBarWidgetState();
}

class _AppBottomBarWidgetState extends State<AppBottomBarWidget> {
  PageEnum selectIndex = PageEnum.home;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 70,
      padding: const EdgeInsets.all(12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _AppBarInfo(
            title: "Головна",
            icon: Icons.house_outlined,
            isSelect: (selectIndex == PageEnum.home),
            onTap: () => onTap(PageEnum.home),
          ),
          _AppBarInfo(
            title: 'Каталог',
            icon: Icons.list,
            isSelect: (selectIndex == PageEnum.catalog),
            onTap: () => onTap(PageEnum.catalog),
          ),
          _AppBarInfo(
            title: 'Кошик',
            icon: Icons.shopping_cart_rounded,
            isSelect: (selectIndex == PageEnum.bucket),
            onTap: () => onTap(PageEnum.bucket),
          ),
          _AppBarInfo(
            title: 'Улюблене',
            icon: Icons.favorite_border,
            isSelect: (selectIndex == PageEnum.favourite),
            onTap: () => onTap(PageEnum.favourite),
          ),
          _AppBarInfo(
            title: 'Кабінет',
            icon: Icons.more_horiz_outlined,
            isSelect: (selectIndex == PageEnum.cabinet),
            onTap: () => onTap(PageEnum.cabinet),
          ),
        ],
      ),
    );
  }

  void onTap(PageEnum index) {
    selectIndex = index;
    setState(() {});
  }
}

class _AppBarInfo extends StatelessWidget {
  const _AppBarInfo(
      {Key? key,
      required this.title,
      required this.icon,
      required this.onTap,
      this.isSelect = false})
      : super(key: key);
  final String title;
  final IconData icon;
  final bool isSelect;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            icon,
            color: isSelect? Colors.blue.shade800: Colors.grey,
            size: 30,
          ),
          Text(
            title,
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 8,
              fontWeight: FontWeight.w500,
            ),
          )
        ],
      ),
    );
  }
}

enum PageEnum{
  home,
  catalog,
  bucket,
  favourite,
  cabinet
}